import com.jhg.sound.Sound;
import com.jhg.sound.SoundManager;

import javax.sound.sampled.AudioFormat;


public class TestSoundPlayer {

    public SoundManager soundManager;
    public Sound sound;

    public static void main(String[] args) {
        TestSoundPlayer soundTest = new TestSoundPlayer();
        soundTest.soundManager = new SoundManager(new AudioFormat(44100, 16, 1, true, false));
        soundTest.sound = soundTest.soundManager.getSound("resources/sounds/fly-bzz.wav");
        try{
        Thread.sleep(1000);
        }
        catch (Exception e){

        }
        soundTest.soundManager.play(soundTest.sound);
        try{
            Thread.sleep(1000);
        }
        catch (Exception e){

        }
        soundTest.soundManager.close();

    }


}
