import com.jhg.tilegame.sprites.Player;
import com.jhg.input.GameActions;
import com.jhg.sound.SoundPlayer;
import com.jhg.test.GameCore;
import com.jhg.input.InputManager;
import com.jhg.ui.GameMenu;
import com.jhg.ui.KeyConfigWindow;

import java.awt.*;

public class Game extends GameCore {

    private Image bgImage;
    private Player player;

    protected InputManager inputManager;
    protected GameActions actions;
    protected GameMenu menu;
    protected KeyConfigWindow keyConfigWindow;

    public static void main(String[] args) {
        Game game = new Game();
        game.run();
    }

    public void init() {

        super.init();
        Window window = screen.getFullScreenWindow();
        inputManager = new InputManager(window);
        // inputManager.setCursor(InputManager.INVISIBLE_CURSOR);
        actions = new GameActions();
        actions.createGameActions(inputManager);
        menu = new GameMenu(screen, actions);
        menu.init();
        keyConfigWindow = new KeyConfigWindow(actions, screen, inputManager);
        keyConfigWindow.init();
        // load background
        bgImage = loadImage("resources/background.jpg");
        // init player
        player = new Player();
        // Create sound player
        soundPlayer = new SoundPlayer();
        soundPlayer.init();

    }


    public void update(long elapsedTime) {
        // check input that can also happen in paused mode
        checkSystemInput();
        if (!isPaused){
            checkGameInput();
            player.update(elapsedTime);
        }
    }

    private void checkGameInput() {
        // update game state
        float vHorizontal = 0;
        float vVertical = 0;

        if (actions.moveLeft.isPressed())
            vHorizontal -= player.getMaxSpeed();
        if (actions.moveRight.isPressed())
            vHorizontal += player.getMaxSpeed();
        if (actions.moveUp.isPressed())
            vVertical -=player.getMaxSpeed();
        if (actions.moveDown.isPressed())
            vVertical += player.getMaxSpeed();
        if (actions.doAction.isPressed())
            soundPlayer.playSound(SoundPlayer.S_ACTION);

        player.updateState(vHorizontal, vVertical);

    }

    private void checkSystemInput() {
        if (actions.pause.isPressed())
            setPaused(!isPaused);
        if (actions.exit.isPressed()){
            stop();
            soundPlayer.close();
        }

        if (actions.configAction.isPressed()) {
            // hide or show the config dialog
            boolean show = !keyConfigWindow.getDialog().isVisible();
            keyConfigWindow.getDialog().setVisible(show);
            setPaused(show);
        }
    }

    private void setPaused(boolean p) {
        if(isPaused != p){
            this.isPaused = p;
            inputManager.resetAllGameActions();
        }
        menu.setPausedState(isPaused);
        soundPlayer.setPaused(p);
    }

    public void draw(Graphics2D g) {
        // draw background
        g.drawImage(bgImage, 0, 0, null);
        // draw player
        g.drawImage(player.getImage(), Math.round(player.getX()), Math.round(player.getY()), null);
        // draw game menu
        menu.draw(g);
    }

}
