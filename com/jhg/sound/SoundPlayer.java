package com.jhg.sound;

import javax.sound.sampled.AudioFormat;

public class SoundPlayer {

    // all game sounds
    public static Sound S_ACTION;
    public static String AMBIENT_FOREST;

    // all game music files
    public static String M_LVL1;

    // music and sound players
    private SoundManager sm;
    private MidiPlayer mp;
    private WavPlayer wp;

    public SoundPlayer(){

    }

    // Load sounds
    public void init(){
        // init sounds and sound player
        sm = new SoundManager(new AudioFormat(44100, 16, 1, true, false));
        S_ACTION = sm.getSound("resources/sounds/action.wav");
        // init the WavPlayer (music)
        wp = new WavPlayer();
        AMBIENT_FOREST = "resources/sounds/ambient.wav";

    }

    public void playSound(Sound sound){
        sm.play(sound);
    }

    public void playMusic(String musicFile, boolean loop){
        wp.play( musicFile, loop);
    }

    public void close(){
        sm.close();
        wp.close();
    }

    public void setPaused(boolean paused){
        sm.setPaused(paused);
        wp.setPaused(paused);
    }

    public void setMusicVolume(float volume){
        wp.setVolume(volume);
    }

}
