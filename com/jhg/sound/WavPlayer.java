package com.jhg.sound;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import java.io.File;

public class WavPlayer {

    private long clipTimePosition;
    private boolean paused;
    Clip clip;

    public WavPlayer(){
        clipTimePosition = 0;
        paused = true;
        clip = null;
    }

    public void play(String filepath, boolean loop){

        try {
            File musicFile = new File(filepath);
            if (musicFile.exists()){
                AudioInputStream audioInput = AudioSystem.getAudioInputStream(musicFile);
                if (clip != null)
                    clip.close();

                clip = AudioSystem.getClip();
                clip.open(audioInput);
                clip.start();
                if (loop)
                    clip.loop(Clip.LOOP_CONTINUOUSLY);
                paused = false;
                clipTimePosition = 0;
            }
            else
            {
                System.out.println("Cant find file: "+filepath);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    public void setPaused(boolean value){
        if (paused != value && clip != null){
            if (value){
                clipTimePosition = clip.getMicrosecondPosition();
                clip.stop();
            }
            else{
                clip.setMicrosecondPosition(clipTimePosition);
                clip.start();
            }
            paused = value;
        }
    }

    public void close(){
        if (clip != null)
            clip.close();
    }


    public void setVolume(float gain){
        if (clip != null){
            FloatControl gainControl =
                    (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
            float dB = (float) (Math.log(gain) / Math.log(10.0) * 20.0);
            gainControl.setValue(dB);
        }

    }


}
