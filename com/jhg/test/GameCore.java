package com.jhg.test;

import com.jhg.graphics.ScreenManager;
import com.jhg.sound.SoundPlayer;

import java.awt.*;
import javax.swing.ImageIcon;

/**
 * Simple abstract class used to test the game by implementing the draw method
 */
public abstract class GameCore {

    protected static final int FONT_SIZE = 24;
    private boolean isRunning;
    protected boolean isPaused;
    protected ScreenManager screen;
    protected SoundPlayer soundPlayer;

    public void init(){
        screen = new ScreenManager();
        Window window = screen.getFullScreenWindow();
        window.setFont(new Font("Dialog", Font.PLAIN, FONT_SIZE));
        window.setBackground(Color.blue);
        window.setForeground(Color.white);
        // allow TAB key as input
        window.setFocusTraversalKeysEnabled(false);
        isRunning = true;
        isPaused = false;
    }

    public void run(){
        try{
            init();
            // soundPlayer.playMusic(SoundPlayer.M_LVL1);
            gameLoop();
        }
        finally{
            screen.restoreScreen();
            lazilyExit();
        }
    }

    public void stop(){
        isRunning = false;
    }

    public Image loadImage(String fileName) {
        return new ImageIcon(fileName).getImage();
    }

    public void gameLoop() {
        long currTime = System.currentTimeMillis();

        while (isRunning) {
            long elapsedTime =
                    System.currentTimeMillis() - currTime;
            currTime += elapsedTime;

            // update the game state
            update(elapsedTime);
            // draw the screen
            Graphics2D g = screen.getGraphics();
            draw(g);
            g.dispose();
            screen.update();
            try {
                Thread.sleep(10);
            }catch(Exception ignored){}

        }
    }

    public void update(long elapsedTime) {
        // do nothing by default
    }

    public abstract void draw(Graphics2D g);

    public void lazilyExit(){
        Thread thread = new Thread(() -> {
            try{
                Thread.sleep(2000);
            }catch (InterruptedException ie){
                System.exit(0);
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

}
