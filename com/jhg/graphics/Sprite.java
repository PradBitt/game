package com.jhg.graphics;

import com.jhg.tilegame.sprites.Player;

import javax.swing.*;
import java.awt.*;

public class Sprite {

    protected Animation currentAnimation;
    // position (pixels)
    private float x;
    private float y;
    // velocity (pixels per millisecond)
    private float dx;
    private float dy;

    public Sprite(){
        currentAnimation = new Animation();
    }

    /**
        Updates this com.jhg.graphics.Sprite's com.jhg.graphics.Animation and its position based
        on the velocity.
    */
    public void update(long elapsedTime) {
        x += dx * elapsedTime;
        y += dy * elapsedTime;
        currentAnimation.update(elapsedTime);
    }

    /**
        Gets this com.jhg.graphics.Sprite's current x position.
    */
    public float getX() {
        return x;
    }

    /**
        Gets this com.jhg.graphics.Sprite's current y position.
    */
    public float getY() {
        return y;
    }

    /**
        Sets this com.jhg.graphics.Sprite's current x position.
    */
    public void setX(float x) {
        this.x = x;
    }

    /**
        Sets this com.jhg.graphics.Sprite's current y position.
    */
    public void setY(float y) {
        this.y = y;
    }

    /**
        Gets this com.jhg.graphics.Sprite's width, based on the size of the
        current image.
    */
    public int getWidth() {
        return currentAnimation.getImage().getWidth(null);
    }

    /**
        Gets this com.jhg.graphics.Sprite's height, based on the size of the
        current image.
    */
    public int getHeight() {
        return currentAnimation.getImage().getHeight(null);
    }

    /**
        Gets the horizontal velocity of this com.jhg.graphics.Sprite in pixels
        per millisecond.
    */
    public float getVelocityX() {
        return dx;
    }

    /**
        Gets the vertical velocity of this com.jhg.graphics.Sprite in pixels
        per millisecond.
    */
    public float getVelocityY() {
        return dy;
    }

    /**
        Sets the horizontal velocity of this com.jhg.graphics.Sprite in pixels
        per millisecond.
    */
    public void setVelocityX(float dx) {
        this.dx = dx;
    }

    /**
        Sets the vertical velocity of this com.jhg.graphics.Sprite in pixels
        per millisecond.
    */
    public void setVelocityY(float dy) {
        this.dy = dy;
    }

    /**
     Gets this Sprite's current image.
     */
    public Image getImage() {
        return currentAnimation.getImage();
    }

    protected Image loadImage(String fileName) {
        return new ImageIcon(fileName).getImage();
    }

    public float getRight(){
        return x + getWidth();
    }
    public float getLeft(){
        return x;
    }
    public float getTop(){
        return y;
    }
    public float getBottom(){
        return y + getHeight();
    }

    /**
     Clones this Sprite. Does not clone position or velocity
     info.
     */
    public Object clone() {
        Sprite clone = new Sprite();
        clone.currentAnimation = currentAnimation;
        return clone;
    }

    public void setCurrentAnimation(Animation a){
        currentAnimation = a;
    }

    public float getCenterX(){
        return getX()+(getWidth()/2);
    }

    public float getCenterY(){
        return getY() + (getHeight()/2);
    }

    public void interact(Player player){

    }

}
