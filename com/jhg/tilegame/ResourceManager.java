package com.jhg.tilegame;

import com.jhg.graphics.Sprite;
import com.jhg.tilegame.sprites.Chicken;
import com.jhg.tilegame.sprites.Item;
import com.jhg.tilegame.sprites.Player;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ResourceManager {

    private ArrayList<Image> tiles;
    private int currentMap;

    private Sprite playerSprite;
    private Sprite chickenSprite;
    private Sprite keySprite;

    /**
     Creates a new ResourceManager with the specified
     GraphicsConfiguration.
     */
    public ResourceManager() {
        loadTileImages();
        initializeHostSprites();
    }

    public void loadTileImages() {
        // keep looking for tile A,B,C, etc. this makes it
        // easy to drop new tiles in the images/directory
        tiles = new ArrayList<>();
        char ch = 'A';
        while (true) {
            String name = "tile_" + ch + ".png";
            File file = new File("resources/images/tiles/" + name);
            if (!file.exists()) {
                break;
            }
            tiles.add(loadImage(name));
            ch++;
        }
    }

    /**
     Gets an image from the images/ directory.
     */
    public Image loadImage(String name) {
        String filename = "resources/images/tiles/" + name;
        return new ImageIcon(filename).getImage();
    }

    public TileMap loadNextMap() {
        TileMap map = null;
        while (map == null) {
            currentMap++;
            try {
                map = loadMap(
                        "resources/maps/Level" + currentMap + ".txt");
            }
            catch (IOException ex) {
                if (currentMap == 1) {
                    // no maps to load!
                    return null;
                }
                currentMap = 0;
                map = null;
            }
        }

        return map;
    }


    private TileMap loadMap(String filename)
            throws IOException
    {
        ArrayList<String> lines = new ArrayList<>();
        int width = 0;
        int height;

        // read every line in the text file into the list
        BufferedReader reader = new BufferedReader(
                new FileReader(filename));
        while (true) {
            String line = reader.readLine();
            // no more lines to read
            if (line == null) {
                reader.close();
                break;
            }

            // add every line except for comments
            if (!line.startsWith("#")) {
                lines.add(line);
                width = Math.max(width, line.length());
            }
        }

        // parse the lines to create a TileEngine
        height = lines.size();
        TileMap newMap = new TileMap(width, height);
        for (int y=0; y<height; y++) {
            String line = lines.get(y);
            for (int x=0; x<line.length(); x++) {
                char ch = line.charAt(x);

                // check if the char represents tile A, B, C etc.
                int tile = ch - 'A';
                if (tile >= 0 && tile < tiles.size()) {
                    newMap.setTile(x, y, new Tile(tiles.get(tile),tile>0));
                }

                // check if the char represents a sprite
                else if (ch == 'k') {
                    newMap.setTile(x, y, new Tile(tiles.get(0),false)); // add tile_A
                    addSprite(newMap, keySprite, x, y);
                }
                else if (ch == 'c') {
                    newMap.setTile(x, y, new Tile(tiles.get(0),false)); // add tile_A
                    addSprite(newMap, chickenSprite, x, y);
                }
                else if (ch == 'p'){
                    newMap.setTile(x, y, new Tile(tiles.get(0),false)); // add tile_A
                    Sprite playerClone = (Sprite)playerSprite.clone();
                    playerClone.setX(TileMapRenderer.tilesToPixels(x));
                    playerClone.setY(TileMapRenderer.tilesToPixels(y));
                    newMap.setPlayer(playerClone);
                }
            }
        }

        return newMap;
    }


    private void addSprite(TileMap map, Sprite hostSprite, int tileX, int tileY)
    {
        if (hostSprite != null) {
            // clone the sprite from the "host"
            Sprite sprite = (Sprite)hostSprite.clone();
            // set position
            sprite.setX(TileMapRenderer.tilesToPixels(tileX));
            sprite.setY(TileMapRenderer.tilesToPixels(tileY));
            // add it to the map

            map.addSprite(sprite);
        }
    }

    public void initializeHostSprites() {
        // create player
        Player player = new Player();
        player.init();
        playerSprite = player;

        Chicken chicken = new Chicken();
        chicken.init();
        chickenSprite = chicken;

        Item.Key key = new Item.Key();
        key.init();
        keySprite = key;

    }
}
