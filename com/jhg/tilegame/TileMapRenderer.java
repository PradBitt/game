package com.jhg.tilegame;

import com.jhg.graphics.Sprite;
import com.jhg.tilegame.sprites.Player;

import java.awt.*;
import java.util.Iterator;

/**
 The TileMapRenderer class draws a TileMap on the screen.
 It draws all tiles, sprites, and an optional background image
 centered around the position of the player.

 <p>If the width of background image is smaller the width of
 the tile map, the background image will appear to move
 slowly, creating a parallax background effect.

 <p>Also, three static methods are provided to convert pixels
 to tile positions, and vice-versa.

 <p>This TileMapRender uses a tile size of 64.
 */
public class TileMapRenderer {

    // private static final int TILE_SIZE = 32;
    // the size in bits of the tile
    // Math.pow(2, TILE_SIZE_BITS) == TILE_SIZE
    private static final int TILE_SIZE_BITS = 5;

    /**
     Converts a pixel position to a tile position.
     */
    public static int pixelsToTiles(float pixels) {
        return pixelsToTiles(Math.round(pixels));
    }

    /**
     Converts a pixel position to a tile position.
     */
    public static int pixelsToTiles(int pixels) {
        // use shifting to get correct values for negative pixels
        return pixels >> TILE_SIZE_BITS;
        // or, for tile sizes that aren't a power of two,
        // use the floor function:
        //return (int)Math.floor((float)pixels / TILE_SIZE);
    }


    /**
     Converts a tile position to a pixel position.
     */
    public static int tilesToPixels(int numTiles) {
        return numTiles << TILE_SIZE_BITS;
        // use this if the tile size isn't a power of 2:
        //return numTiles * TILE_SIZE;
    }

    /**
     Draws the specified TileMap.
     */
    public void draw(Graphics2D g, TileMap map,
                     int screenWidth, int screenHeight)
    {
        Player player = (Player)map.getPlayer();
        int mapWidth = tilesToPixels(map.getWidth());
        int mapHeight = tilesToPixels(map.getHeight());

        // get the scrolling position of the map
        // based on player's position
        int cameraPositionX = Math.round(player.getX()) - screenWidth/2;
        cameraPositionX = Math.max(cameraPositionX, 0);
        cameraPositionX = Math.min(cameraPositionX, mapWidth - screenWidth);

        // get the y offset to draw all sprites and tiles
        int cameraPositionY = Math.round(player.getY()) - screenHeight/2;
        cameraPositionY = Math.max(cameraPositionY, 0);
        cameraPositionY = Math.min(cameraPositionY, mapHeight - screenHeight);

        g.setColor(Color.black);
        g.fillRect(0, 0, screenWidth, screenHeight);

        // draw the visible tiles
        int firstTileX = pixelsToTiles(cameraPositionX);
        int lastTileX = firstTileX +
                pixelsToTiles(screenWidth) + 1;

        int firstTileY = pixelsToTiles(cameraPositionY);
        int lastTileY = firstTileY +
                pixelsToTiles(screenHeight) + 2;

        for (int y=firstTileY; y<lastTileY; y++) {
            for (int x=firstTileX; x <= lastTileX; x++) {
                Tile tile = map.getTile(x, y);
                if (tile != null) {
                    g.drawImage(tile.getImage(),
                            tilesToPixels(x) - cameraPositionX,
                            tilesToPixels(y) - cameraPositionY,
                            null);
                }
            }
        }

        // draw sprites
        Iterator<Sprite> i = map.getSprites();
        while (i.hasNext()) {
            Sprite sprite = i.next();
            int x = Math.round(sprite.getX()) - cameraPositionX;
            int y = Math.round(sprite.getY()) - cameraPositionY;
            g.drawImage(sprite.getImage(), x, y, null);
        }
        // draw player
        g.drawImage(player.getImage(),
                Math.round(player.getX()) - cameraPositionX,
                Math.round(player.getY()) - cameraPositionY,
                null);

        // draw player bag
        player.drawBag(g, screenWidth, screenHeight);
    }

}
