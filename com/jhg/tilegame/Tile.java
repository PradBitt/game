package com.jhg.tilegame;

import java.awt.*;

public class Tile {
    private final Image image;
    private final boolean isSolid;
    public Tile(Image i, boolean solid){
        image = i;
        isSolid = solid;
    }

    public Image getImage() {
        return image;
    }

    public boolean isSolid() {
        return isSolid;
    }
}
