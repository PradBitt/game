package com.jhg.tilegame.sprites;

import com.jhg.graphics.Animation;
import com.jhg.graphics.Sprite;

import java.lang.reflect.Constructor;

public abstract class Creature extends Sprite {

    protected static final String CREATURES_SPRITE_PATH = "resources/images/sprites/creatures/";

    public static final int NUM_STATES = 7;

    public static final int STATE_STAND_STILL = 0;
    public static final int STATE_RUN_RIGHT = 1;
    public static final int STATE_RUN_DOWN = 2;
    public static final int STATE_RUN_LEFT = 3;
    public static final int STATE_RUN_UP = 4;
    public static final int STATE_DYING = 5;
    public static final int STATE_DEAD = 6;

    protected Animation[] animations;
    private int state;
    private float maxSpeed;
    protected long stateTime;
    protected static final float DIE_TIME = 1.0f;


    public Creature() {
        this.state = STATE_STAND_STILL;
        animations = new Animation[NUM_STATES];
    }

    public void setMaxSpeed(float speed){
        maxSpeed = speed;
    }

    public float getMaxSpeed(){return maxSpeed;}

    public void setState(int newState){
        if (this.state != newState){
            this.state = newState;
            currentAnimation = this.animations[state];
            currentAnimation.reset();
            stateTime = 0;
            if (state == STATE_DYING){
                setVelocityX(0);
                setVelocityY(0);
            }
        }
    }

    @Override
    public Object clone() {
        // use reflection to create the correct subclass
        Constructor<?> constructor = getClass().getConstructors()[0];
        Animation[] animationsClone = new Animation[NUM_STATES];
        for (int i = 0; i< animations.length; i++)
            animationsClone[i] = (Animation)animations[i].clone();
        try {
            Object creatureClone = constructor.newInstance();
            Creature c = (Creature)creatureClone;
            c.setAnimations(animationsClone);
            c.maxSpeed = maxSpeed;
            if (creatureClone instanceof Player){
                Player p = (Player)creatureClone;
                p.setBag(((Player)this).getBag());
            }
            return creatureClone;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void setAnimations(Animation[] newAnimations){
        if (newAnimations != null && newAnimations.length == NUM_STATES){
            animations = newAnimations;
            currentAnimation = animations[STATE_STAND_STILL];
        }
    }

    public boolean stateIs(int s){
        return s == state;
    }

    @Override
    public void update(long elapsedTime) {
        stateTime += elapsedTime;
        this.currentAnimation.update(elapsedTime);
        // update to "dead" state
        if (state == STATE_DYING && stateTime >= DIE_TIME) {
            setState(STATE_DEAD);
        }
    }

    public float getNextX(long elapsedTime){
        if (state == STATE_RUN_RIGHT)
            return getX()+maxSpeed*elapsedTime;
        if (state == STATE_RUN_LEFT)
            return getX()-maxSpeed*elapsedTime;
        return getX();
    }
    public float getNextY(long elapsedTime){
        if (state == STATE_RUN_DOWN)
            return getY()+maxSpeed*elapsedTime;
        if (state == STATE_RUN_UP)
            return getY()-maxSpeed*elapsedTime;
        return getY();
    }

    public boolean isDead(){
        return state == STATE_DEAD || state == STATE_DYING;
    }

    /**
     * Has to be implemented to initialize the
     * creature animation
     */
    public abstract void init();

    /**
     * Has to be implemented to set/change
     * the creature state -> this ist the
     * AI of the creature
     */
    public abstract void updateState();


}
