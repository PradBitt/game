package com.jhg.tilegame.sprites;

import com.jhg.graphics.Animation;

import java.awt.*;

public class Chicken extends Creature{

    public Chicken(){
        super();
    }

    @Override
    public void init() {
        setX(0);
        setY(0);
        setMaxSpeed(0.02f);
        animations = new Animation[NUM_STATES];
        Image stand = loadImage(CREATURES_SPRITE_PATH + "chicken.png");
        Animation standAnim = new Animation();
        standAnim.addFrame(stand,1000);
        currentAnimation = standAnim;
        animations[STATE_STAND_STILL] = standAnim;
        animations[STATE_RUN_RIGHT] = standAnim;
        animations[STATE_RUN_DOWN] = standAnim;
        animations[STATE_RUN_LEFT] = standAnim;
        animations[STATE_RUN_UP] = standAnim;
        animations[STATE_DYING] = standAnim;
        animations[STATE_DEAD] = standAnim;
    }

    public void updateState(){
        if (stateTime > 10000){
            if (stateIs(STATE_STAND_STILL))
                setState(STATE_RUN_LEFT);
            else if (stateIs(STATE_RUN_LEFT))
                setState(STATE_RUN_UP);
            else if (stateIs(STATE_RUN_UP))
                setState(STATE_RUN_RIGHT);
            else if (stateIs(STATE_RUN_RIGHT))
                setState(STATE_RUN_DOWN);
            else
                setState(STATE_STAND_STILL);
        }
    }
}
