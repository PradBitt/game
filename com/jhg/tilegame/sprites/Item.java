package com.jhg.tilegame.sprites;
import com.jhg.graphics.Animation;
import com.jhg.graphics.Sprite;

import java.awt.*;
import java.lang.reflect.Constructor;

/**
 A PowerUp class is a Sprite that the player can pick up.
 */
public abstract class Item extends Sprite {

    protected static final String ITEM_SPRITE_PATH = "resources/images/sprites/objects/";

    public Object clone() {
        // use reflection to create the correct subclass
        Constructor constructor = getClass().getConstructors()[0];
        try {
            Object clone = constructor.newInstance();
            Sprite s = (Sprite)clone;
            s.setCurrentAnimation((Animation)currentAnimation.clone());
            return clone;
        }
        catch (Exception ex) {
            // should never happen
            ex.printStackTrace();
            return null;
        }
    }

    /**
     A Key Item. Gives the Player the ability to open something.
     */
    public static class Key extends Item {
        public Key() {
        }
        public void init(){
            Image keyImage = loadImage(ITEM_SPRITE_PATH + "key.png");
            currentAnimation.addFrame(keyImage,1000);
        }
    }

}
