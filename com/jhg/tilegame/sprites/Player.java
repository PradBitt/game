package com.jhg.tilegame.sprites;

import com.jhg.graphics.Animation;
import com.jhg.graphics.Sprite;
import com.jhg.utils.Bag;

import java.awt.*;

public class Player extends Creature{

    private static final String SPRITE_PATH = "resources/images/sprites/player/";
    private Bag bag;

    public Player(){
        super();
    }

    @Override
    public void init(){
        this.setX(0);
        this.setY(0);
        this.setMaxSpeed(0.05f);
        bag = new Bag(4);
        // ***** Create state animations *****> //
        // create STATE_STAND_STILL animation
        Image playerStand = loadImage(SPRITE_PATH + "playerDown.png");
        Animation standAnim = new Animation();
        standAnim.addFrame(playerStand,1000);
        currentAnimation = standAnim;
        animations[STATE_STAND_STILL] = standAnim;

        // create STATE_RUN_RIGHT animation
        Image playerRunRight1 = loadImage(SPRITE_PATH + "playerRunRight1.png");
        Image playerRunRight2 = loadImage(SPRITE_PATH + "playerRunRight2.png");
        Image playerRunRight3 = loadImage(SPRITE_PATH + "playerRunRight3.png");
        Animation runRightAnim = new Animation();
        runRightAnim.addFrame(playerRunRight1, 200);
        runRightAnim.addFrame(playerRunRight2, 200);
        runRightAnim.addFrame(playerRunRight3, 200);
        animations[STATE_RUN_RIGHT] = runRightAnim;

        // create STATE_RUN_DOWN animation
        Image playerRunDown1 = loadImage(SPRITE_PATH + "playerRunDown1.png");
        Image playerRunDown2 = loadImage(SPRITE_PATH + "playerRunDown2.png");
        Image playerRunDown3 = loadImage(SPRITE_PATH + "playerRunDown3.png");
        Animation runDownAnim = new Animation();
        runDownAnim.addFrame(playerRunDown1, 200);
        runDownAnim.addFrame(playerRunDown2, 200);
        runDownAnim.addFrame(playerRunDown3, 200);
        animations[STATE_RUN_DOWN] = runDownAnim;

        // create STATE_RUN_LEFT animation
        Image playerRunLeft1 = loadImage(SPRITE_PATH + "playerRunLeft1.png");
        Image playerRunLeft2 = loadImage(SPRITE_PATH + "playerRunLeft2.png");
        Image playerRunLeft3 = loadImage(SPRITE_PATH + "playerRunLeft3.png");
        Animation runLeftAnim = new Animation();
        runLeftAnim.addFrame(playerRunLeft1, 200);
        runLeftAnim.addFrame(playerRunLeft2, 200);
        runLeftAnim.addFrame(playerRunLeft3, 200);
        animations[STATE_RUN_LEFT] = runLeftAnim;

        // create STATE_RUN_UP animation
        Image playerRunUp1 = loadImage(SPRITE_PATH + "playerRunUp1.png");
        Image playerRunUp2 = loadImage(SPRITE_PATH + "playerRunUp2.png");
        Image playerRunUp3 = loadImage(SPRITE_PATH + "playerRunUp3.png");
        Animation runUpAnim = new Animation();
        runUpAnim.addFrame(playerRunUp1, 200);
        runUpAnim.addFrame(playerRunUp2, 200);
        runUpAnim.addFrame(playerRunUp3, 200);
        animations[STATE_RUN_UP] = runUpAnim;

        // create STATE_DEAD animation
        Image playerDead = loadImage(SPRITE_PATH + "playerDead.png");
        Animation deadAnim = new Animation();
        deadAnim.addFrame(playerDead,1000);
        animations[STATE_DEAD] = deadAnim;

        // set STATE_DEAD animation
        animations[STATE_DYING] = deadAnim;
    }

    @Override
    public void updateState() {

    }

    public void updateState(float new_velX, float new_velY){
        if (new_velX == 0 && new_velY == 0)
            this.setState(Player.STATE_STAND_STILL);
        else if (this.getVelocityX() != new_velX) {
            if (new_velX > 0)
                this.setState(Player.STATE_RUN_RIGHT);
            else if (new_velX < 0)
                this.setState(Player.STATE_RUN_LEFT);
            else if (new_velX == 0) {
                if (new_velY < 0)
                    this.setState(Player.STATE_RUN_UP);
                else
                    this.setState(Player.STATE_RUN_DOWN);
            }
        }
        else if (this.getVelocityY() != new_velY) {
            if (new_velY > 0)
                this.setState(Player.STATE_RUN_DOWN);
            else if (new_velY < 0)
                this.setState(Player.STATE_RUN_UP);
            else if (new_velY == 0) {
                if (new_velX < 0)
                    this.setState(Player.STATE_RUN_LEFT);
                else
                    this.setState(Player.STATE_RUN_RIGHT);
            }
        }
        this.setVelocityX(new_velX);
        this.setVelocityY(new_velY);
    }

    public boolean isRunning() {
        return (stateIs(STATE_RUN_LEFT) || stateIs(STATE_RUN_DOWN)
                || stateIs(STATE_RUN_RIGHT) || stateIs(STATE_RUN_UP));
    }

    public void drawBag(Graphics2D g, int screenWidth, int screenHeight){
        if (bag != null)
            bag.draw(g, screenWidth, screenHeight);
    }

    public boolean takeItem(Sprite item){
        return bag.addItem(item);
    }

    public Bag getBag(){
        return bag;
    }

    public void setBag(Bag b){
        this.bag = b;
    }

}