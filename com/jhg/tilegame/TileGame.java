package com.jhg.tilegame;

import com.jhg.graphics.Sprite;
import com.jhg.tilegame.sprites.Creature;
import com.jhg.tilegame.sprites.Item;
import com.jhg.tilegame.sprites.Player;
import com.jhg.input.GameActions;
import com.jhg.input.InputManager;
import com.jhg.sound.SoundPlayer;
import com.jhg.test.GameCore;
import com.jhg.utils.Fps;
import com.jhg.utils.Message;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;

public class TileGame extends GameCore {

    private TileMap map;
    // private ResourceManager resourceManager;
    protected InputManager inputManager;
    protected GameActions actions;
    private TileMapRenderer renderer;
    private final Point collisionPointCache = new Point();
    private Fps fps;
    private Sprite interactionSprite;
    private Message message;

    public static void main(String[] args) {
        TileGame game = new TileGame();
        game.run();
    }

    public void init() {
        super.init();
        Window window = screen.getFullScreenWindow();
        renderer = new TileMapRenderer();
        inputManager = new InputManager(window);
        inputManager.setCursor(InputManager.INVISIBLE_CURSOR);
        // define game actions
        actions = new GameActions();
        actions.createGameActions(inputManager);
        // start resource manager
        // resourceManager = new ResourceManager();
        // load first map
        map = new ResourceManager().loadNextMap();
        // Create sound player
        soundPlayer = new SoundPlayer();
        soundPlayer.init();
        soundPlayer.playMusic(SoundPlayer.AMBIENT_FOREST, true);
        soundPlayer.setMusicVolume(0.3f);
        fps = new Fps();
        message = new Message("Welcome to chicken island!", 4000);
    }

    public void update(long elapsedTime) {
        // check input that can also happen in paused mode
        fps.update(elapsedTime);
        message.update(elapsedTime);
        checkSystemInput();
        if (!isPaused){
            // update player
            checkGameInput(); // update player intern state based on game input
            updatePosition(map.getPlayer(), elapsedTime); // update position/state based on velocity
            map.getPlayer().update(elapsedTime); // update stateTime and animation
            // update other sprites
            Iterator<Sprite> i = map.getSprites();
            while (i.hasNext()) {
                Sprite sprite = i.next();
                if (sprite instanceof Creature) {
                    Creature creature = (Creature)sprite;
                    creature.updateState();
                    updatePosition(creature, elapsedTime);
                    }
                // normal update
                sprite.update(elapsedTime);
            }
        }
    }

    private void setInteractionSprite() {
        interactionSprite = null;
        if (map.getPlayer().isRunning()){
            ArrayList<Sprite> potentialInteractionSprites = new ArrayList<>();
            Player player = map.getPlayer();
            if (player.stateIs(Creature.STATE_RUN_RIGHT))
                potentialInteractionSprites = getSpriteCollision(player,player.getX()+10,player.getY());
            else if (player.stateIs(Creature.STATE_RUN_LEFT))
                potentialInteractionSprites = getSpriteCollision(player,player.getX()-10,player.getY());
            else if (player.stateIs(Creature.STATE_RUN_DOWN))
                potentialInteractionSprites = getSpriteCollision(player,player.getX(),player.getY()+10);
            else if (player.stateIs(Creature.STATE_RUN_UP))
                potentialInteractionSprites = getSpriteCollision(player,player.getX(),player.getY()-10);
            if (potentialInteractionSprites.size() == 0)
                interactionSprite = null;
            else if (potentialInteractionSprites.size() > 0){
                interactionSprite = potentialInteractionSprites.get(0);
                float playerCenterX = player.getCenterX();
                float playerCenterY = player.getCenterY();
                for (int i = 1; i < potentialInteractionSprites.size(); i++) {
                    if (player.stateIs(Player.STATE_RUN_RIGHT) || player.stateIs(Player.STATE_RUN_LEFT)){
                        if (Math.abs(playerCenterY-potentialInteractionSprites.get(i).getCenterY()) < Math.abs(playerCenterY-interactionSprite.getCenterY()))
                            interactionSprite = potentialInteractionSprites.get(i);
                    }
                    else{
                        if (Math.abs(playerCenterX-potentialInteractionSprites.get(i).getCenterX()) < Math.abs(playerCenterX-interactionSprite.getCenterX()))
                            interactionSprite = potentialInteractionSprites.get(i);
                    }
                }
            }
        }
    }


    private void checkGameInput() {
        Player player = map.getPlayer();
        // update game state
        float vHorizontal = 0;
        float vVertical = 0;

        if (actions.moveLeft.isPressed())
            vHorizontal -= player.getMaxSpeed();
        if (actions.moveRight.isPressed())
            vHorizontal += player.getMaxSpeed();
        if (actions.moveUp.isPressed())
            vVertical -=player.getMaxSpeed();
        if (actions.moveDown.isPressed())
            vVertical += player.getMaxSpeed();
        if (actions.doAction.isPressed())
            tryToInteract();

        player.updateState(vHorizontal, vVertical);
    }

    private void tryToInteract() {
        setInteractionSprite();
        if (interactionSprite != null){
            if (interactionSprite instanceof Item){
                if (map.getPlayer().takeItem(interactionSprite)){
                    map.removeSprite(interactionSprite);
                    soundPlayer.playSound(SoundPlayer.S_ACTION);
                }
                else{
                    message.setText("Bag is full!");
                }

            }

        }

    }

    private void checkSystemInput() {
        if (actions.pause.isPressed())
            setPaused(!isPaused);
        if (actions.exit.isPressed()){
            stop();
            soundPlayer.close();
        }
    }

    private void setPaused(boolean p) {
        if(isPaused != p){
            this.isPaused = p;
            inputManager.resetAllGameActions();
        }
        soundPlayer.setPaused(p);
    }

    @Override
    public void draw(Graphics2D g) {

        renderer.draw(g, map, screen.getWidth(), screen.getHeight());
        g.drawString("FPS: "+fps.getFps(),10,20);
        message.draw(g,screen.getWidth(),screen.getHeight());
    }


    private void updatePosition(Creature creature, long elapsedTime ){
        // ******** position update considering collisions *******
        float newX = creature.getNextX(elapsedTime);
        float newY = creature.getNextY(elapsedTime);
        Point collisionTile = getTileCollision(creature, newX, newY);


        if (collisionTile != null) {
            if (creature.stateIs(Creature.STATE_RUN_RIGHT))
                newX = TileMapRenderer.tilesToPixels(collisionTile.x) - creature.getWidth();
            else if (creature.stateIs(Creature.STATE_RUN_LEFT))
                newX = TileMapRenderer.tilesToPixels(collisionTile.x + 1);
            else if (creature.stateIs(Creature.STATE_RUN_DOWN))
                newY = TileMapRenderer.tilesToPixels(collisionTile.y) - creature.getHeight();
            else if (creature.stateIs(Creature.STATE_RUN_UP))
                newY = TileMapRenderer.tilesToPixels(collisionTile.y + 1);
        }

        ArrayList<Sprite> collisionSprites = getSpriteCollision(creature, newX, newY);
        if (collisionSprites.size() != 0) {
            if (creature.stateIs(Creature.STATE_RUN_RIGHT))
                newX = collisionSprites.get(0).getLeft() - creature.getWidth();
            else if (creature.stateIs(Creature.STATE_RUN_LEFT))
                newX = collisionSprites.get(0).getRight();
            else if (creature.stateIs(Creature.STATE_RUN_DOWN))
                newY = collisionSprites.get(0).getTop() - creature.getHeight();
            else if (creature.stateIs(Creature.STATE_RUN_UP))
                newY = collisionSprites.get(0).getBottom();
        }

        creature.setX(newX);
        creature.setY(newY);
    }

    /**
     Gets the tile that a Sprites collides with. Only the
     Sprite's X or Y should be changed, not both. Returns null
     if no collision is detected.
     */
    public Point getTileCollision(Sprite sprite,
                                  float newX, float newY)
    {
        float fromX = Math.min(sprite.getX(), newX);
        float fromY = Math.min(sprite.getY(), newY);
        float toX = Math.max(sprite.getX(), newX);
        float toY = Math.max(sprite.getY(), newY);

        // get the tile locations
        int fromTileX = TileMapRenderer.pixelsToTiles(fromX);
        int fromTileY = TileMapRenderer.pixelsToTiles(fromY);
        int toTileX = TileMapRenderer.pixelsToTiles(
                toX + sprite.getWidth() - 1);
        int toTileY = TileMapRenderer.pixelsToTiles(
                toY + sprite.getHeight() - 1);

        // check each tile for a collision
        for (int x=fromTileX; x<=toTileX; x++) {
            for (int y=fromTileY; y<=toTileY; y++) {
                if (x < 0 || x >= map.getWidth() ||
                        map.getTile(x, y) == null || map.getTile(x, y).isSolid())
                {
                    // collision found, return the tile
                    collisionPointCache.setLocation(x, y);
                    return collisionPointCache;
                }
            }
        }
        // no collision found
        return null;
    }

    private ArrayList<Sprite> getSpriteCollision(Creature creature, float newX, float newY) {

        ArrayList<Sprite> collisionSprites = new ArrayList<>();
        // run through the list of Sprites
        Iterator<Sprite> i = map.getSprites();
        while (i.hasNext()) {
            Sprite otherSprite = i.next();
            if (isCollision(creature, newX, newY, otherSprite)) {
                if (collisionSprites.size() == 0)
                    collisionSprites.add(otherSprite);
                else { // collision wit more than one creature -> get first collision
                    if (creature.stateIs(Creature.STATE_RUN_RIGHT)) {
                        if (otherSprite.getLeft() < collisionSprites.get(0).getLeft()) {
                            collisionSprites = new ArrayList<>();
                            collisionSprites.add(otherSprite);
                        } else if (otherSprite.getLeft() == collisionSprites.get(0).getLeft())
                            collisionSprites.add(otherSprite);
                    } else if (creature.stateIs(Creature.STATE_RUN_LEFT)) {
                        if (otherSprite.getRight() > collisionSprites.get(0).getRight()) {
                            collisionSprites = new ArrayList<>();
                            collisionSprites.add(otherSprite);
                        } else if (otherSprite.getRight() == collisionSprites.get(0).getRight())
                            collisionSprites.add(otherSprite);
                    } else if (creature.stateIs(Creature.STATE_RUN_UP)) {
                        if (otherSprite.getBottom() > collisionSprites.get(0).getBottom()) {
                            collisionSprites = new ArrayList<>();
                            collisionSprites.add(otherSprite);
                        } else if (otherSprite.getBottom() == collisionSprites.get(0).getBottom())
                            collisionSprites.add(otherSprite);
                    } else if (creature.stateIs(Creature.STATE_RUN_DOWN)) {
                        if (otherSprite.getTop() < collisionSprites.get(0).getTop()) {
                            collisionSprites = new ArrayList<>();
                            collisionSprites.add(otherSprite);
                        } else if (otherSprite.getTop() == collisionSprites.get(0).getTop())
                            collisionSprites.add(otherSprite);
                    }
                }
            }
        }
        if (isCollision(creature, newX, newY, map.getPlayer())) {
            // collision creature with player found, return the player
            collisionSprites.add(map.getPlayer());
        }
        return collisionSprites;
    }

    /**
     Checks if two Sprites collide with one another. Returns
     false if the two Sprites are the same. Returns false if
     one of the Sprites is a Creature that is not alive.
     */
    public boolean isCollision(Sprite s1, float newX, float newY, Sprite s2) {
        // if the Sprites are the same, return false
        if (s1 == s2) {
            return false;
        }

        // if one of the Sprites is a dead Creature, return false
        if (s1 instanceof Creature && ((Creature)s1).isDead()) {
            return false;
        }
        if (s2 instanceof Creature && ((Creature)s2).isDead()) {
            return false;
        }

        // get the pixel location of the Sprites
        int s1x = Math.round(newX);
        int s1y = Math.round(newY);
        int s2x = Math.round(s2.getX());
        int s2y = Math.round(s2.getY());

        // check if the two sprites' boundaries intersect
        return (s1x < s2x + s2.getWidth() &&
                s2x < s1x + s1.getWidth() &&
                s1y < s2y + s2.getHeight() &&
                s2y < s1y + s1.getHeight());
    }

}
