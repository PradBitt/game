package com.jhg.input;

import java.awt.event.KeyEvent;

public class GameActions {

    public GameAction configAction;
    public GameAction moveRight;
    public GameAction moveDown;
    public GameAction moveLeft;
    public GameAction moveUp;
    public GameAction pause;
    public GameAction exit;

    public GameAction doAction;

    public void createGameActions(InputManager im){
        moveRight = new GameAction("move right");
        im.mapToKey(moveRight, KeyEvent.VK_D);
        moveDown = new GameAction("move down");
        im.mapToKey(moveDown, KeyEvent.VK_S);
        moveLeft = new GameAction("move left");
        im.mapToKey(moveLeft, KeyEvent.VK_A);
        moveUp = new GameAction("move up");
        im.mapToKey(moveUp, KeyEvent.VK_W);
        pause = new GameAction("pause game");
        im.mapToKey(pause, KeyEvent.VK_P);
        exit = new GameAction("exit game");
        im.mapToKey(exit, KeyEvent.VK_ESCAPE);
        configAction = new GameAction("config");

        doAction = new GameAction("test", GameAction.DETECT_INITIAL_PRESS_ONLY);
        im.mapToKey(doAction, KeyEvent.VK_SPACE);


    }



}
