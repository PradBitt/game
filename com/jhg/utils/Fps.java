package com.jhg.utils;

import java.util.LinkedList;

/*
Class to calculate the frames per second.
 */
public class Fps {

    LinkedList<Long> elapsedTimes;
    long timeSinceUpdate;
    long fps;

    public Fps() {
        elapsedTimes = new LinkedList<>();
        fps = 0;
        timeSinceUpdate = 0;
    }

    public void update(long elapsedTime){
        timeSinceUpdate += elapsedTime;
        elapsedTimes.addLast(elapsedTime);
        if (elapsedTimes.size() > 10)
            elapsedTimes.removeFirst();
    }

    // get a new average value (10 frames) every second
    public long getFps() {
        if (elapsedTimes.size()==10 && timeSinceUpdate > 1000){
            long sum = 0;
            for (long time : elapsedTimes) {
                sum += time;
            }
            fps = 10000 / sum;
            timeSinceUpdate = 0;
        }
        return fps;
    }
}
