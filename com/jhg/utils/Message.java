package com.jhg.utils;

import java.awt.*;

public class Message {

    String text;
    long maxShowTime;
    long currentShowTime;

    public Message(String s, long timeInMS){
        text = s;
        maxShowTime = timeInMS;
        currentShowTime = 0;
    }

    public void update(long elapsedTime){
        currentShowTime += elapsedTime;
        if (currentShowTime > maxShowTime){
            currentShowTime = 0;
            text = null;
        }
    }

    public void draw(Graphics2D g, int screenWidth, int screenHeight){
        if (text != null){
            g.drawString(text,screenWidth/4,screenHeight/2);
        }
    }

    public void setText(String s){
        text = s;
        currentShowTime = 0;
    }

}
