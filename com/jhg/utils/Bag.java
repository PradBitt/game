package com.jhg.utils;

import com.jhg.graphics.Sprite;

import java.awt.*;
import java.util.LinkedList;

public class Bag {
    private int spaces;
    LinkedList<Sprite> content = new LinkedList<>();

    public Bag(int maxSpaces){
        this.spaces = maxSpaces;
    }

    public boolean addItem(Sprite item){
        if (content.size() < spaces){
            content.addLast(item);
            return true;
        }
        return false;
    }

    public void setSpaces(int newMaxSpaces){
        spaces = Math.max(spaces,newMaxSpaces);
    }

    public void draw(Graphics2D g, int screenWidth, int screenHeight){
        int x = 0;
        for (Sprite item : content){
            g.drawImage(item.getImage(),x,screenHeight - item.getHeight(),null);
            x += item.getWidth();
        }

    }

}
